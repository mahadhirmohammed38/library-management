package com.library.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.entity.BookEntity;
import com.library.repository.BooksRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class BooksServiceImpl implements BooksService {
	
	@Autowired
	BooksRepository booksRepository;
	
	@Override
	public BookEntity createBook(BookEntity book) {
		BookEntity savedBook = booksRepository.save(book);
		return savedBook;
	}

	@Override
	public Optional<BookEntity> readBook(long imei) {
		Optional<BookEntity> existingBook = booksRepository.findById(imei);
		return existingBook;
	}

	@Override
	public BookEntity updateBook(BookEntity book) {
		BookEntity updatedBook = booksRepository.save(book);
		return updatedBook;
	}

	@Override
	public void deleteBook(long imei) {
		booksRepository.deleteById(imei);
	}

	@Override
	public Iterable<BookEntity> getAllBooks() {
		return booksRepository.findAll();
	}

}
