package com.library.service;

import java.util.Optional;

import com.library.entity.BookEntity;

public interface BooksService {
	
	BookEntity createBook(BookEntity book);
	
	Optional<BookEntity> readBook(long imei);
	
	BookEntity updateBook(BookEntity book);
	
	void deleteBook(long imei);
	
	Iterable<BookEntity> getAllBooks();
}
