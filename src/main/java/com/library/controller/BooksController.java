package com.library.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.library.entity.BookEntity;
import com.library.service.BooksService;

@Controller
@RequestMapping(path = "/library")
public class BooksController {
	
	@Autowired
	private BooksService booksService;
	
	@Value("${app.message}")
	private String welcomeMessage;
	
	@PostMapping(path = "/add")
	public @ResponseBody BookEntity addBook(@RequestParam String bookName, @RequestParam String bookAuthor) {
		BookEntity newBook = new BookEntity();
		newBook.setBookName(bookName);
		newBook.setBookAuthor(bookAuthor);
		return booksService.createBook(newBook);
		
	}

	@GetMapping(path = "/get")
	public @ResponseBody Optional<BookEntity> readBook(@RequestParam long imei) {
		return booksService.readBook(imei);
	}
	
	@PostMapping(path = "/update")
	public @ResponseBody BookEntity updateBook(long imei, String bookName, String bookAuthor) {
		BookEntity book = new BookEntity();
		book.setImeiId(imei);
		book.setBookName(bookName);
		book.setBookAuthor(bookAuthor);
		return booksService.updateBook(book);
	}
	
	
	@PostMapping(path = "/delete")
	public @ResponseBody String deleteBook(long imei) {
		booksService.deleteBook(imei);
		return "Success";
	}	
	
	@GetMapping(path = "/listAll")
	public @ResponseBody Iterable<BookEntity> listAllBooks(){
		System.out.println(booksService.getAllBooks());
		return booksService.getAllBooks();
	}
	
	@GetMapping(path = "/welcome")
	public @ResponseBody String getWelcome() {
		return welcomeMessage;
	}
	
//	
//	@GetMapping(path = "/listAll", produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Iterable<BookEntity>> listAllBooks() {
//	    Iterable<BookEntity> books = booksService.getAllBooks();
//	    System.out.println(ResponseEntity.ok().body(books));
//	    return ResponseEntity.ok().body(books);
//	}

}
