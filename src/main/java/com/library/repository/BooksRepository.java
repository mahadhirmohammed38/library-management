package com.library.repository;

import org.springframework.data.repository.CrudRepository;

import com.library.entity.BookEntity;

public interface BooksRepository extends CrudRepository<BookEntity, Long> {
	
}
