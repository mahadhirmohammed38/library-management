package com.library.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "books_repo")
public class BookEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long imeiId;

	private String bookName;

	private String bookAuthor;

	public long getImeiId() {
		return imeiId;
	}

	public void setImeiId(long imeiId) {
		this.imeiId = imeiId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	@Override
	public String toString() {
		return "BookEntity [imeiId=" + imeiId + ", bookName=" + bookName + ", bookAuthor=" + bookAuthor + "]";
	}

}
