package com.library.controller;

import java.util.concurrent.atomic.AtomicInteger;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.library.entity.BookEntity;
import com.library.service.BooksService;

import jakarta.transaction.Transactional;

@Transactional
@SpringBootTest
class BooksControllerTest {

	@Autowired
	private BooksService booksService;

	@Test
	void testAddBook() {
		System.err.println("testAddBook()...");
		BookEntity newBook = new BookEntity();
		newBook.setBookAuthor("Test Author 1");
		newBook.setBookName("Test Book name 1");

		Long imei = (Long) booksService.createBook(newBook).getImeiId();
		System.err.println(imei);
		Assertions.assertThat(imei).isNotNull();
	}

	@Test
	void testReadBook() {
		System.err.println("testReadBook()...");
		Assertions.assertThat(booksService.readBook(2)).isNotNull();
	}

	@Test
	void testUpdateBook() {
		System.err.println("testUpdateBook()...");
		BookEntity book = new BookEntity();
		book.setImeiId(2);
		book.setBookName("Test Book 3");
		book.setBookAuthor("Test Author 3");
		Assertions.assertThat(booksService.updateBook(book).getImeiId()).isNotNull();
	}

	@Test
	void testListAllBooks() {
		System.err.println("testListAllBooks()...");
		AtomicInteger count=new AtomicInteger(0);
		booksService.getAllBooks().forEach(book -> count.incrementAndGet());
		Assertions.assertThat(count.get()).isNotZero();

	}

}
