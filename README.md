
# Spring Boot and REST API - Training

This project is created to demonstrate and implement Spring Boot and REST API learnings into practice.

  

#### Session 1 - June 6, 2023

##### **Overview**

- Overview of training program
	- Self learning resources
	- Hands-on Exercises
	- Capstone Project / Business Case study
- Deliverables to be completed
	- Complete all Hands-on exercises
	- Complete Capstone Project
- Softwares required
	- JDK 1.8/17/18/19/20
	- Eclipse IDE
	- MySQL Server and Workbench
	- Postman
	- Maven
	
##### **Hands-on 1**
- [x] Implement simple Spring core project
- [x] Integrate basic CRUD operations with Hibernate or JPA
- [x] Write unit test cases for each operation

Refer [this commit](https://gitlab.com/mahadhirmohammed38/library-management/-/tree/b573d5d2e74ac432f2ef026e87973f8abf71c02f) for completed version of this hands-on exercise.

#### Session 2 - June 9, 2023
##### **Overview**
- Intro to Spring Boot
- Advantages of Spring Boot over Spring Core

##### **Hands-on 2**
- [x] Build Spring Boot application using Maven and run the application from command line using Java as JAR
	- [x] This can be done by `java -jar .\target\library-management-0.0.1-SNAPSHOT.jar`
- [ ] Configure profiles in Spring Boot application
- [ ] Read the properties for different profiles and print respective output when the application is run from different profiles
